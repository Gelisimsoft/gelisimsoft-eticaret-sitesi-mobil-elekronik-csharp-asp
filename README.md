# Gelişimsoft E-Ticaret Sitesi (Mobil & Elekronik)

![Alt text](https://raw.githubusercontent.com/Gelisimsoft/Gelisimsoft-ETicaret-Sitesi-Mobil-Elekronik-CSharp-ASP/master/Gelisimsoft.com_E-Ticaret_Sitesi_1.JPG
)

## GİRİŞ BİLGİLERİ ##
Kullanıcı adı	: info@gelisimsoft.com

Şifre		: demo 

## MODÜLLER ##


* Anasayfa
* Hakkımızda
* Banka Hesapları
* Tedairkçi Başvurusu
* İletişim
* Kargo Bilgileri
* Kurumsal Satış
* Sipariş Takip
* Ödeme Bildirim Formu
* İade / Değişim Formu
* Satış Sözleşmesi
* Ödeme Seçenekleri
* Kargo Takibi
* Güvenli E- Ticaret
* Üyelik Sözleşmesi
* Koşulsuz İade
* Üyelik İşlemleri (Üyel Ol ve Giriş Sayfaları)
* Üyelik İşlemleri (Sosyal Medya Hesabı İle Giriş/Üye Olma)
* Üyelik İşlemleri – Sepetiniz
* Üyelik İşlemleri – Sipariş Takip
* Üyelik İşlemleri – Kargo Takip
* Üyelik İşlemleri – Kurye Takip
* Üyelik İşlemleri – Ödeme Bildirim Formu
* Üyelik İşlemleri - İade/Değişim Formu
* Yönetim Paneli - Kategori İşlemleri
* Yönetim Paneli - Kategori Düzenleme
* Yönetim Paneli - Ürün Ekleme
* Yönetim Paneli - Ürün Düzenleme
* Yönetim Paneli - Ürün Silme
* Yönetim Paneli - Ürün Kopyalama
* Yönetim Paneli - Ürün Resim Ekleme
* Yönetim Paneli - Ürün Taksitlendirme
* Yönetim Paneli - Ürün Listesi
* Yönetim Paneli - Yeni Siparişler
* Yönetim Paneli - Bekleyen Siparişler
* Yönetim Paneli - Tamamlanan Siparişler
* Yönetim Paneli - İptal Edilen Siparişler
* Yönetim Paneli - İade Edilen Siparişler
* Yönetim Paneli - Ödeme Bildirimleri
* Yönetim Paneli - Kargo İşlemleri
* Yönetim Paneli - Kurye İşlemleri
* Yönetim Paneli - Ürün Talepleri
* Yönetim Paneli - İade Değişim Bildirimleri
* Yönetim Paneli - Yeni Siparişler (Hemen Al)
* Yönetim Paneli - Bekleyen Siparişler (Hemen Al)
* Yönetim Paneli - Tamamlanan Siparişler (Hemen Al)
* Yönetim Paneli - İptal Edilen Siparişler (Hemen Al)
* Yönetim Paneli - İade Edilen Siparişler (Hemen Al)
* Yönetim Paneli - İade Değişim Bildirimleri (Hemen Al)
* Yönetim Paneli - Kargo İşlemleri (Hemen Al)
* Yönetim Paneli - Kurye İşlemleri (Hemen Al)
* Yönetim Paneli - E-Bülten Üye Listesi
* Yönetim Paneli - Excel&#39;den Toplu Aktar
* Yönetim Paneli - Stokda Kalmayan Ürünler
* Yönetim Paneli - Stokda Kalmayan Ürünler Listesi
* Yönetim Paneli - Satışı Yapılmayacak Ürünler
* Yönetim Paneli - Satışı Yapılmayacak Ürünler Listesi
* Yönetim Paneli - Üye Yönetimi
* Yönetim Paneli - Silinen Ürünler
* Yönetim Paneli - Anasayfa Haber Sistemi
* Yönetim Paneli - Şifre İşlemleri
* Yönetim Paneli - Mesaj Gönder
* Yönetim Paneli - İletişim Mesajları


www.gelisimsoft.com 

info@gelisimsoft.com
